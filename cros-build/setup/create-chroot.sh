#!/bin/bash

set -e

env

echo "device: $CROS_DEVICE"
chroot_dir=$HOME/chroot-"$CROS_DEVICE"
cache_dir=$HOME/cache

if [ -n "${CROS_PROJECT}" ]; then
    IFS=',' read -ra PROJECTS <<<"$CROS_PROJECT"

    # let the user select the project when more than one option is available
    if [ "${#PROJECTS[@]}" -gt 1 ]; then
        echo "Select project:"
        select p in "${PROJECTS[@]}"; do
            project=${p}
            break
        done
    else
        project="${PROJECTS[0]}"
    fi

    echo "Setup ${project} project..."
    cp /home/cros-build/chromiumos/src/scripts/${CROS_DEVICE}_files/.gitcookies /home/cros-build

    args=""
    if [ "${CROS_CFG_CIPD}" = "true" ]; then
        args="--program ${CROS_PROGRAM:-$CROS_DEVICE} --project ${project} --branch ${CROS_SDK_BRANCH}"
        if [ -n "${CROS_CHIPSET}" ]; then
            args="$args --chipset ${CROS_CHIPSET}"
        fi
    else
        args="${CROS_PROGRAM:-$CROS_DEVICE} ${project} ${CROS_SDK_BRANCH}"
    fi

    src/config/setup_project.sh $args

    repo sync --force-sync -j4
    repo start def --all
fi

echo "Creating CrOS SDK chroot..."
cros_sdk \
    --enter \
    --nouse-image \
    --no-ns-pid \
    --debug \
    --chroot "$chroot_dir" \
    --cache-dir "$cache_dir" \
    $@

exit 0
