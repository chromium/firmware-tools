#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=zork

image_variants=("morphius" "gumboz" "berknip")

cmd_setup() {
    if [ $# -lt 1 ] || [[ ! " ${image_variants[*]} " =~ " ${1} " ]]; then
        echo "Missing or invalid Chromebook codename"
        echo "Usage: $(basename "$0") setup <$(
            IFS=$'|'
            echo "${image_variants[*]}"
        )>"
        exit 1
    fi

    setup_board --board=$BOARD --profile=$1
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cros_workon --board ${BOARD} start depthcharge
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-zork-13434.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    if [ $# -lt 1 ] || [[ ! " ${image_variants[*]} " =~ " ${1} " ]]; then
        echo "Missing or invalid Chromebook codename"
        echo "Usage: $(basename "$0") build <$(
            IFS=$'|'
            echo "${image_variants[*]}"
        )>"
        exit 1
    fi

    fw_path=/build/${BOARD}/firmware/image-"$1".dev.bin

    # Build Depthcharge
    emerge-${BOARD} depthcharge

    # Build bootimage
    FW_NAME=$1 emerge-${BOARD} chromeos-bootimage
    cp --verbose "$fw_path" firmware/${BOARD}-"$1"-new.bin
}


cmd_$@

exit 0
