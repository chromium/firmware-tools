#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=cherry

cmd_setup() {
    setup_board --board=$BOARD
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cros_workon --board ${BOARD} start depthcharge
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-cherry-14454.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    # Build depthcharge
    cros_workon --board ${BOARD} start depthcharge
    USE=fwconsole FW_NAME=${BOARD_VARIANT} emerge-${BOARD} depthcharge

    # Build FW image
    cros_workon --board ${BOARD} start chromeos-bootimage
    FW_NAME=${BOARD_VARIANT} emerge-${BOARD} chromeos-bootimage

    cp --verbose /build/${BOARD}/firmware/image-${BOARD_VARIANT}.dev.bin firmware/${BOARD}-new.bin
}

cmd_$1

exit 0
