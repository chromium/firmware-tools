#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=brya
export BOARD_VARIANT=volmar

cmd_setup() {
    setup_board --board=$BOARD --profile=$BOARD_VARIANT
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-brya-14505.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    # Build depthcharge
    cros_workon --board ${BOARD} start depthcharge
    FW_NAME=${BOARD_VARIANT} emerge-${BOARD} depthcharge

    # Build FW image
    cros_workon --board ${BOARD} start chromeos-bootimage
    FW_NAME=${BOARD_VARIANT} emerge-${BOARD} chromeos-bootimage

    cp --verbose /build/${BOARD}/firmware/image-${BOARD_VARIANT}.dev.bin firmware/brya-new.bin
}

cmd_$@

exit 0
