#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=guybrush
export BOARD_VARIANT=dewatt

cmd_setup() {
    setup_board --board=$BOARD --profile=dewatt
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-guybrush-14500.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    # Build Depthcharge
    cros_workon --board ${BOARD} start depthcharge
    emerge-${BOARD} depthcharge

    # Build bootimage
    FW_NAME=${BOARD_VARIANT} emerge-${BOARD} chromeos-bootimage

    cp --verbose /build/${BOARD}/firmware/image-${BOARD_VARIANT}.dev.bin firmware/guybrush-new.bin
}

cmd_$@

exit 0
