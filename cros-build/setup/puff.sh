#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=puff
export BOARD_VARIANT=kaisa

cmd_setup() {
    setup_board --board=$BOARD --profile=$BOARD_VARIANT --force
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-puff-13324.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    # Build depthcharge
    cros_workon --board ${BOARD} start depthcharge
    FW_NAME=${BOARD_VARIANT} emerge-${BOARD} depthcharge
}

cmd_image() {
    local output=firmware/${BOARD}-new.bin

    cp firmware/${BOARD}.bin ${output}
    cbfstool \
        ${output} \
        remove \
        -n fallback/payload \
        -r COREBOOT
    cbfstool \
        ${output} \
        add-payload \
        -r COREBOOT \
        -n fallback/payload \
        -f /build/${BOARD}/firmware/${BOARD}/depthcharge/dev.elf

    ls -l ${output}
}

cmd_$@

exit 0
