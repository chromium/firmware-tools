#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=zork
export PROFILE=jelboz
export BOARD_VARIANT=shuboz

cmd_setup() {
    setup_board --board=$BOARD --profile=$PROFILE
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cros_workon --board ${BOARD} start depthcharge
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-zork-13434.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    FW_NAME=${BOARD_VARIANT} emerge-${BOARD} chromeos-bootimage

    cp --verbose /build/${BOARD}/firmware/image-${BOARD_VARIANT}.dev.bin firmware/dalboz-new.bin
}

cmd_$@

exit 0
