#!/usr/bin/env python

from xmlrpc import client
import argparse
import csv
import os
import subprocess
import sys
import time


CFG_FIELDS = ['name', 'sn', 'port', 'board', 'model']

class Device(object):

    def __init__(self, device, device_name, dryrun):
        self._device = device
        self._device_name = device_name
        self._servod_uri = f"http://0.0.0.0:{self._device['port']}"
        self._dryrun=dryrun

    @property
    def name(self):
        return self._device_name

    @property
    def sn(self):
        return self._device['sn']

    @property
    def board(self):
        return self._device['board']

    @property
    def dryrun(self):
        return self._dryrun

    def _run(self, cmd):
        if (self.dryrun):
            print("* {}".format(cmd))
            return
        if subprocess.call(cmd, shell=True):
            raise Exception("Command failed")

    def _get_control(self, ctrl):
        if (self.dryrun):
            print(f'get {ctrl}')
            return

        servo_client = client.ServerProxy(self._servod_uri, verbose=False)

        return servo_client.get(ctrl)

    def _set_control(self, ctrl, value, sleep=1):
        if (self.dryrun):
            print(f'set {ctrl}:{value}')
            return

        servo_client = client.ServerProxy(self._servod_uri, verbose=False)
        servo_client.set(ctrl, value)

        if sleep:
            time.sleep(sleep)

    def _flag(self, name, status):
        self._set_control(name, "on" if status else "off")

    def _reset(self, status):
        self._flag("cold_reset", status)

    def _spi2_vref(self, status, voltage="pp1800"):
        self._set_control("spi2_vref", voltage)
        self._set_control("spi2_buf_en", "on" if status else "off")

    def _flashrom(self, cmd):
        run = "flashrom --programmer raiden_debug_spi:serial={} {}".format(
            self.sn, cmd)
        self._run(run)

    def _read(self, path):
        self._flashrom("-r {}".format(path))

    def _erase(self):
        self._flashrom("-E")

    def _write(self, path):
        self._flashrom("-w {}".format(path))

    def flash(self, fw_path=None, backup_path=None):
        raise NotImplementedError("No run() method implemented")


class ServoMicro(Device):

    def flash(self, fw_path=None, backup_path=None):
        voltage = "pp3300" if self.board == "sarien" else "pp1800"

        self._reset(False)
        self._reset(True)
        self._spi2_vref(True, voltage)

        if backup_path:
            self._read(backup_path)

        if fw_path:
            self._flag("fw_wp_en", True)
            self._flag("fw_wp", False)
            self._flag("fw_up", True)
            time.sleep(1)
            self._erase()
            time.sleep(1)
            self._write(fw_path)
            time.sleep(1)

        self._spi2_vref(False, voltage)
        self._flag("cpu_uart_en", True)
        self._flag("dev_mode", True)
        self._reset(False)


class SuzyQ(Device):

    def _flashrom(self, cmd):
        run = "\
flashrom --programmer raiden_debug_spi:target=AP,serial={} {}".format(
            self.sn, cmd)
        self._run(run)

    def flash(self, fw_path=None, backup_path=None):
        if backup_path:
            self._read(backup_path)
        if fw_path:
            self._write(fw_path)


DEVICE_CLS = {
    'octopus': SuzyQ,
    'grunt': SuzyQ,
    'hana': ServoMicro,
    'kevin': ServoMicro,
    'coral': SuzyQ,
    'rammus': SuzyQ,
    'jacuzzi': SuzyQ,
    'nami': SuzyQ,
    'hatch': SuzyQ,
    'sarien': ServoMicro,
    'trogdor': SuzyQ,
    'volteer': SuzyQ,
    'dedede': SuzyQ,
    'zork': SuzyQ,
    'asurada': SuzyQ,
    'cherry': SuzyQ,
    'puff': SuzyQ,
    'guybrush': SuzyQ,
    'nissa': SuzyQ,
    'brya': SuzyQ,
    'brask': SuzyQ,
    'skyrim': SuzyQ
    'corsola': SuzyQ
}


def get_device(config, device_name, dryrun):
    device = next((item for item in config if item["name"] == device_name), None)
    if device is None:
        return None
    print(device)
    cls = DEVICE_CLS[device['board']]
    return cls(device, device_name, dryrun)


def set_local_env():
    cwd = os.getcwd()
    bin_path, lib_path = (os.path.join(cwd, x) for x in ['bin', 'lib'])
    os.environ.update({
        env: ':'.join([path, os.environ.get(env, '')])
        for env, path in [('PATH', bin_path), ('LD_LIBRARY_PATH', lib_path)]
    })


def get_config(config_path):
    all_boards = []

    # Parse configuration file; skip lines beginning with #,
    # ignore white spaces and empty lines
    with open(config_path) as rcfile:
        conf = csv.reader(
            filter(lambda row: row[0] != '#', rcfile), skipinitialspace=True)
        for row in conf:
            if row:
                all_boards.append(dict(zip(CFG_FIELDS, row)))

    return all_boards

def main(argv):
    parser = argparse.ArgumentParser("Servo flashing tool for Chromebooks")
    parser.add_argument("--config", default="/etc/google-docker-servo.conf",
                        help="Path to the servod file")
    parser.add_argument("--device",
                        help="Name of the device to flash")
    parser.add_argument("--firmware",
                        help="Path to the firmware file to flash")
    parser.add_argument("--backup",
                        help="Path to a backup firmware file")
    parser.add_argument("--dryrun",
                        help="Print the commands without executing them",
                        action=argparse.BooleanOptionalAction)
    args = parser.parse_args(argv[1:])

    set_local_env()
    config = get_config(args.config)

    if not args.device:
        print("Please choose one device:")
        for c in config:
            print("-  {}".format(c['name']))
    else:
        device = get_device(config, args.device, args.dryrun)
        if device is None:
            print("Device not found: {}".format(args.device))
            sys.exit(1)

        print("Device: {}".format(device.name))
        print("Serial: {}".format(device.sn))
        device.flash(args.firmware, args.backup)


if __name__ == '__main__':
    main(sys.argv)
