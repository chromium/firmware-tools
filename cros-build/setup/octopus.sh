#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=octopus

cmd_setup() {
    patch -d/ --forward -p0 < octopus_files/0001-libcxx-7.0.0.ebuild.patch
    ./setup_board --board=$BOARD
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cros_workon --board ${BOARD} start depthcharge
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-octopus-11297.83.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    emerge-${BOARD} depthcharge
}

cmd_image() {
    local output=firmware/${BOARD}-new.bin

    cp firmware/${BOARD}.bin ${output}
    cbfstool \
        ${output} \
        remove \
        -n fallback/payload \
        -r COREBOOT
    cbfstool \
        ${output} \
        add-payload \
        -r COREBOOT \
        -n fallback/payload \
        -f /build/octopus/firmware/depthcharge/dev.elf

    ls -l ${output}
}

cmd_$1

exit 0
