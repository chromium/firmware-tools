#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=trogdor

image_variants=("lazor" "kingoftown")

cmd_setup() {
    setup_board --board=${BOARD}
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cros_workon --board ${BOARD} start depthcharge
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-trogdor-13577.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    if [ $# -lt 1 ] || [[ ! " ${image_variants[*]} " =~ " ${1} " ]]; then
        echo "Missing or invalid Chromebook codename"
        echo "Usage: $(basename "$0") build <$(
            IFS=$'|'
            echo "${image_variants[*]}"
        )>"
        exit 1
    fi

    fw_path=/build/${BOARD}/firmware/image-"$1".dev.bin

    FW_NAME=$1 emerge-${BOARD} chromeos-bootimage
    cp --verbose "$fw_path" firmware/${BOARD}-"$1"-new.bin
}

cmd_$@

exit 0
