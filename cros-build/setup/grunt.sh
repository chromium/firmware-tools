#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=grunt

image_variants=("barla" "careena" "kasumi360")

cmd_setup() {
    ./setup_board --board=$BOARD
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cros_workon --board ${BOARD} start depthcharge
    pushd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-grunt-11031.B-collabora
    git checkout FETCH_HEAD
    popd
}

cmd_build() {
    emerge-${BOARD} depthcharge
}

cmd_image() {
    if [ $# -lt 1 ] || [ ! -L firmware/${BOARD}-"$1".bin ]; then
        echo "Missing or invalid Chromebook codename"
        echo "Usage: $(basename "$0") image <$(IFS=$'|'; echo "${image_variants[*]}" )>"
        exit 1
    fi

    fw_path=firmware/${BOARD}-${1}.bin

    local output=${fw_path%.bin}-new.bin

    cp ${fw_path} ${output}
    cbfstool \
        ${output} \
        remove \
        -n fallback/payload \
        -r COREBOOT
    cbfstool \
        ${output} \
        add-payload \
        -r COREBOOT \
        -n fallback/payload \
        -f /build/grunt/firmware/grunt/depthcharge/dev.elf

    ls -l ${output}
}

cmd_$@

exit 0
