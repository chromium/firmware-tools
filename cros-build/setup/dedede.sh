#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=dedede
export PROFILE=magolor
export BOARD_VARIANT=dedede_alc1015_amp

cmd_setup() {
    setup_board --board=$BOARD --profile=$PROFILE --force
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cros_workon --board ${BOARD} start depthcharge
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-dedede-13606.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    USE="$PROFILE" emerge-${BOARD} depthcharge
}

cmd_build_ec() {
    cros_workon --board ${BOARD} start chromeos-ec
    USE="$PROFILE" emerge-${BOARD} chromeos-ec
}

cmd_image() {
    local output=firmware/${BOARD}-new.bin

    cp firmware/${BOARD}.bin ${output}
    cbfstool \
        ${output} \
        remove \
        -n fallback/payload \
        -r COREBOOT
    cbfstool \
        ${output} \
        add-payload \
        -r COREBOOT \
        -n fallback/payload \
        -f /build/${BOARD}/firmware/${BOARD_VARIANT}/depthcharge/dev.elf

    ls -l ${output}
}

cmd_image_ec() {
    local output=firmware/${BOARD}-ec-new.bin

    cp /build/${BOARD}/firmware/${PROFILE}/ec.bin ${output}
    ls -l ${output}
}

cmd_$1

exit 0
